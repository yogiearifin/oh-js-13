// this is a looping
// given an array that has the name of all main antagonist of JoJo's Bizzare Adventure series
// create a function that can print all of the villain name and their respective parts in the console
// Input -> villain()
// Output -> Part 1 antagonist is Dio Brando, Part 2 antagonist is Kars, etc.

const villain = [
  "Dio Brando",
  "Kars",
  "DIO",
  "Kira Yoshikage",
  "Diavolo",
  "Enrico Pucci",
  "Funny Valentine",
  "Akefu Satoru",
];

function Antagonist() {
  // write the code here
  for (i = 0; i < villain.length; i++) {
    console.log(`Part ${i + 1} antagonist is ${villain[i]}`);
}
}

Antagonist()
// variable -> global scope, can be reassigned and redefined
// var car = "honda";
// console.log("1", car); // honda
// car = "suzuki";
// console.log("2", car); // suzuki

// // let -> local scope, can be reassigned, but cannot be redefined
// let color = "biru";
// console.log("1", color); // biru
// color = "merah";
// console.log("2", color); // merah
// // const -> local scope, cannot be reassigned not redefined
// const price = "1 milyar rupiah";
// console.log("1", price); // 1 milyar rupiah
// // price = "seribu rupiah"; ERROR

// // function printMyCar(brand, warna, harga) {
// //   console.log(
// //     "aku punya mobil merknya " +
// //       brand +
// //       " warnanya " +
// //       warna +
// //       " dan harganya " +
// //       harga
// //   );
// // }

// // const dataDariBackend = {
// //   id: 1,
// //   harga: 1000,
// //   name: "jam tangan murah",
// // };

// // const ongkir = 5000;

// // total harga = harga + ongkir

// // function sumHarga(harga, ongkosKirim) {
// //   return "total harga anda adalah Rp " + (harga + ongkosKirim);
// // }

// // console.log(sumHarga(dataDariBackend.harga, ongkir));

// // console.log("return car outside function", car);
// // console.log("return color outside function", color);
// // console.log("return price outside function", price);

// // printMyCar(car, color, price);

// //operator
// // 1 math
// // 2 comparison
// // 3 logical
// // 4 assignment
// // 5 conditional

// //math -> basic math operation

// // // plus -> + to add value
// console.log(1 + 2); //3
// console.log(-1 + 4); //3

// // //minus -> - to decrease value
// console.log(5 - 2); // 3
// console.log(-4 - -1); //3

// // //times -> * to multiply value
// console.log(20 * 3); //60
// console.log(-15 * 10); // 150

// // //divide -> / to devide value
// console.log(20 / 5); // 4
// console.log(-60 / -3); // 20

// // //modulus -> % check for modulus
// console.log(15 % 2); //1 tidak habis
// console.log(10 % 5); //0 habis

// // increment -> ++ increase by 1
// let inc = 1;
// console.log(++inc);

// // decrement -> -- decrease by 1
// let dec = 3;
// console.log(--dec);

// // comparison -> comparing two or more items
// const a = 1;
// const b = 2;

// const number = 1; // int data type
// const angka = "1"; // string data type

// const bigNumber = 6;
// const smallNumber = 2;
// const smallerNumber = 1;

// // equal -> only compare value
// console.log(a == b);

// // equal strict -> compare value and data type
// console.log(number == angka); // check only value
// console.log(number === angka); // check value && data type

// // more than
// console.log(b > a);

// // less than
// console.log(a < b);

// // more than or equal to
// console.log(b >= smallNumber);

// // less than or equal to
// console.log(a <= bigNumber);

// // not equal
// console.log(number !== angka);

// // logical -> return boolean (true or false)

// // and -> both must be true in order to be true
// console.log(b > a && a < b);
// // true && true -> true
// console.log(b > a && bigNumber < b);
// // true && false -> false

// // or -> if one of the option is true then return true
// console.log(b > a || a > b);
// // true || false -> true
// console.log(b < a || bigNumber > smallerNumber);
// //false || true -> true

// // not -> revese of the return
// console.log(!(a > b)); // return is not (true) -> false
// console.log(!(b > a)); // return is not (false) -> true

// // assignment -> giving or manipulation a value to or from a variable

// // assigning -> =
// const assignment = "value";
// // variableType variableName = variableValue

// // plus assignmet -> += sum
// let num = 3;
// let mod = 10;

// console.log((num += 2));

// // minus assignment -> -= decreasing value
// console.log((num -= 3));

// // times assignment -> *=
// console.log((num *= 3));

// // divide assignment -> /=
// console.log((num /= 3));

// // modulus assignment -> %=
// console.log((mod %= 2));

// // conditional -> like if else -> ? (also be called ternary operator)

// const x = 3;
// const y = 10;
// const optional =
//   x > y ? "x lebih besar dari y" : x === 3 ? "x adalah 3" : "x kurang dari y";
// console.log(optional);

// // data type

// // 1 primitive
// //   a. string
// //   b. number / integer
// //   c. boolean
// //   d. null
// //   e. undefined
// //   f. NaN -> not a number

// // 2 non-primitive
// //   a. object
// //   b. date
// //   c. array

// //primitive data type

// //string -> use quote ' or double quote ""

// let string = "this is string";
// let str = "1";
// let hello = "hello";
// let world = "world";
// console.log(string);
// console.log(string + " " + str);
// console.log(hello + " " + world);
// console.log(hello - world);

// // number
// let t = 100;
// console.log(t + t);
// console.log(t + str);

// // boolean -> true or false
// console.log(true);
// console.log(false);
// // truthy or falsy value
// console.log("truthy or falsy", 1 + 1 ? true : false);

// const backendData = {
//   id: 1,
//   name: "barang",
//   harga: 0,
// };

// function cekDataDariBackend(item) {
//   if (item) {
//     return "item ada";
//   } else {
//     return "item tidak ada";
//   }
// }

// console.log(cekDataDariBackend(backendData.name));
// console.log(cekDataDariBackend(backendData.harga));

//null -> state of data that has no value
const n = null;
const zero = 0;
// console.log(n, zero);

// undefined -> state of data that hasn't been defined yet
let undef;
// console.log(undef);

// non-primitive

// object -> {} -> an item that can store a key and a value

let mobil = {
  // key : value
  warna: "biru",
  lampu: "bulat",
  kursi: 4,
  merk: "hyundai",
  harga: "200 juta",
  _id: 1,
};

// console.log("aku punya mobil merknya " + mobil.merk);
// console.log("harga mobilku adalah " + mobil["harga"]);
// console.log(mobil["_id"]);

// date -> let d = new Date() -> for anything date or time related
let now = new Date();
// console.log(now);
// console.log(now.getHours())

// array -> [] -> an item that can store a value in an index

const queue = ["budi", "toni", "mike", "joko", "sani"];

// console.log(mobil);
// console.log(queue[2]);

// const listMobilFromBackend = [
//   {
//     // key : value
//     warna: "biru",
//     lampu: "bulat",
//     kursi: 4,
//     merk: "hyundai",
//     harga: "200 juta",
//     _id: 1,
//   },
//   {
//     // key : value
//     warna: "merah",
//     lampu: "kotak",
//     kursi: 6,
//     merk: "Tesla",
//     harga: "800 juta",
//     _id: 2,
//   },
//   {
//     // key : value
//     warna: "biru",
//     lampu: "hexagon",
//     kursi: 16,
//     merk: "toyota",
//     harga: "900 juta",
//     _id: 3,
//   }
// ]

// const listMerkMobil = listMobilFromBackend.map((mobil,index)=>{
//   return mobil.merk
// })

// console.log(listMerkMobil)

// console.log(listMobilFromBackend[1].merk)

// array method

// const fruits = ["apple", "mango", "guava", "durian", "dragon fruit"];
// console.log(fruits);

// // pop -> remove last element of an array
// fruits.pop();
// console.log(fruits);

// // push -> add an element to the last index of an array
// fruits.push("pear");
// console.log(fruits);

// // sort -> sort elements of an array
// fruits.sort();
// console.log(fruits);

// // reverse -> reverse the order of an array
// fruits.reverse();
// console.log(fruits);

// // concat -> merge two or more existing arrays
// const vegetable = ["lettuce", "tomato", "carrots"];
// const topping = ["jelly", "boba", "biscuit"];
// const juice = fruits.concat(vegetable, topping);
// console.log(juice);

// join -> to join every value in an array to become a string
const hello = ["hello", "world", 5];
console.log(hello.join("-"));

// function

// function has paramater -> parameter will be manipulated in function code
// parameter can be any data type (string,num/int,array,obj), besides from null, undefined, NaN
function greet(name) {
  return "hello " + name;
}

const moreGreeting = (name, age) => {
  return `hello ${name}, your age is ${age}`;
};

console.log(greet("yogie"));
console.log(moreGreeting("yogie", 26));

const sumAllNumber = (
  number1 = 0,
  number2 = 0,
  number3 = 0,
  number4 = 0,
  number5 = 0
) => {
  return number1 + number2 + number3 + number4 + number5;
};

console.log(sumAllNumber(1, 2, 3, 4));
console.log(sumAllNumber(10, 20, 30, 40));
console.log(sumAllNumber(10, 20, 30, 40, 50));

const timesNumber = (num1, num2) => {
  console.log(num1 * num2);
};

timesNumber(2, 2); // invoking -> calling the function

const arr = [0, 1, 2, 3];
const obj = {
  name: "danu",
  age: 13,
  hobby: "playing games",
};

const newObj = {
  name: "ari",
  age: 18,
  hobby: "playing guitar",
  isGraduated: true,
};

const firstIndex = (array) => {
  return array[0];
};
console.log(firstIndex(arr));

// conditional -> if else -> if(condition) {some function} else {some function}

const checkIfGraduated = (object) => {
  if (object["isGraduated"]) {
    return `${object["name"]} is already graduated`;
  } else {
    return `${object["name"]} is not graduated`;
  }
};

console.log(checkIfGraduated(obj));
console.log(checkIfGraduated(newObj));

const checkIsAdult = (object) => {
  if (object["age"] >= 18) {
    return `${object["name"]} is already an adult`;
  } else if (object["age"] === 13) {
    return `${object["name"]} is 13 years old`;
  } else {
    return `${object["name"]} is not an adult`;
  }
};

console.log(checkIsAdult(obj));
console.log(checkIsAdult(newObj));

// switch case -> switch (condition) { case condition1 : some function ; default : some default action}

// console.log(now.getDay());
// 4 -> kamis
const getDateString = (date) => {
  let day = "";
  switch (date) {
    case 0:
      day = "Minggu";
      break;
    case 1:
      day = "Senin";
      break;
    case 2:
      day = "Selasa";
      break;
    case 3:
      day = "Rabu";
      break;
    case 4:
      day = "Kamis";
      break;
    case 5:
      day = "Jumat";
      break;
    case 6:
      day = "Sabtu";
      break;
    default:
      day = "aku tak tau hari ini hari apa bro";
  }
  return day;
};

// console.log(getDateString(now.getDay()));
// console.log(getDateString(4));
// console.log(getDateString(0));
// console.log(now.toLocaleString())
// console.log(now);

// looping

// 1 for
// do while
// while

// for loop
const nums = 5;

// for (let i = 0; i < nums; i++) {
//   console.log(i);
// }

let iterator = 1;
do {
  iterator++;
  console.log(iterator);
} while (iterator <= nums);
console.log("----");
let i = 1;
while (i <= nums) {
  i++;
  console.log(i);
}

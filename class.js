// class -> basic of OOP (Object Oriented Programmming)

// class Fruit {
//   constructor(name, price, taste) {
//     this.name = name;
//     this.price = price;
//     this.taste = taste;
//   }
//   get getName() {
//     // getter -> to return a value
//     return `the fruit name is ${this.name}`;
//   }
//   get getPrice() {
//     return `the fruit price is ${this.price}`;
//   }
//   get getTaste() {
//     return `the fruit taste is ${this.taste}`;
//   }
//   set setName(newName) {
//     // setter -> to set a value
//     this.name = newName;
//   }
//   set setPrice(newPrice) {
//     this.price = newPrice;
//   }
//   set setTaste(newTaste) {
//     this.taste = newTaste;
//   }
// }

// const durian = new Fruit("Durian", 50000, "Good");
// console.log(durian);
// console.log(durian.getName);
// console.log(durian.getPrice);
// console.log(durian.getTaste);
// durian.setTaste = "bad";
// console.log(durian.getTaste);

// class Juice extends Fruit {
//   constructor(name, price, taste, isMix, size) {
//     // 3 of the constructors are from the parent
//     super(name, price, taste); // parent's constructors
//     this.isMix = isMix;
//     this.size = size;
//   }
//   get getIsMix() {
//     return `the juice is ${
//       this.isMix ? "the juice is mixed" : "the juice is pure"
//     }`;
//   }
//   get getJuiceName() {
//     return `the juice name is ${this.name}`;
//   }
//   get getJuicePrice() {
//     return `the juice name is ${this.price}`;
//   }
//   get getSize() {
//     return `the juice is ${this.size}`;
//   }
//   set setIsMixed(newMixed) {
//     this.isMix = newMixed;
//   }
//   set setSize(newSize) {
//     this.size = newSize;
//   }
// }

// const orangeJuice = new Juice("Orange Juice", 10000, "Sweet", false, "Medium");
// console.log(orangeJuice);
// console.log(orangeJuice.getJuiceName);
// orangeJuice.setPrice = 5000;
// console.log(orangeJuice.getJuicePrice);
// orangeJuice.setSize = "Extra Large";
// console.log(orangeJuice.getSize);

// class Smoothie extends Juice {
//   constructor(name, price, taste, isMix, size, smoothness) {
//     super(name, price, taste, isMix, size);
//     this.smoothness = smoothness;
//   }
//   get getSmoothness() {
//     return `the smoothie is ${this.smoothness}`;
//   }
//   set setSmoothness(newSmoothness) {
//     this.smoothness = newSmoothness;
//   }
// }

// const strawberrySmoothie = new Smoothie(
//   "Strawberry Smoothie",
//   15000,
//   "Sweet",
//   true,
//   "Large",
//   "extra smooth"
// );

// console.log(strawberrySmoothie);
// console.log(strawberrySmoothie.getPrice);
// console.log(strawberrySmoothie.getSize);
// console.log(strawberrySmoothie.getSmoothness);
// strawberrySmoothie.setPrice = 7500;
// strawberrySmoothie.setSize = "Medium";
// console.log(strawberrySmoothie.getPrice);
// console.log(strawberrySmoothie.getSize);

// class Laptop {
//   constructor(brand, price, power) {
//     this.brand = brand;
//     this.price = price;
//     this.power = power;
//     this.cekPriceBrand = function (number) {
//       return price * number;
//     };
//     this.total = this.price * this.power;
//   }
//   set setPrice(newPrice) {
//     this.price = newPrice;
//   }
// }

// // const Lenovo = new Laptop("Lenovo", 1000, 700);
// // console.log(Lenovo.total);
// // Lenovo.setPrice = 100;
// // console.log(Lenovo.total);

// inheritance -> inheriting properties from parent to child


// encapsulatioon -> to protect properties from outside interferance -> by declaring the property as variable
function Laptop(power) {
  var price = 5000; // declaring property as variable
  var total = function () { // data being formulated
    return power * 100 + price;
  };
  this.totalPrice = function () {
    return total(); // abstraction -> hiding the formula, but instead only return the result
  };
}

const Lenovo = new Laptop(700);
Lenovo.price = 10000; // trying to change the price
console.log(Lenovo.totalPrice()); // same price

console.log(Lenovo.totalPrice()) // abstraction result

// polymorphism -> taking a property from parent, if the child does not have the said property

class People {
  constructor(name) {
    this.name = name;
  }
  greet() {
    return `Hello my name is ${this.name}`;
  }
}

class Actors extends People {
  constructor(name) {
    super(name);
  }
  // greet() {
  //   return `Hello my name is ${this.name} and I am famous`;
  // }
  // Actors object will take greet function from People (parent)
}

const VinDiesel = new Actors("Vin Diesel");
console.log(VinDiesel.greet());

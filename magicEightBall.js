// Magic Eight Ball is one of the famous toys in the US. People play it by asking a yes/no question then the ball will pick a an answer
// make a function that accepts a yes/no question as the parameter
// the function will return the question and a random answer from response array
// example
// input -> magicEightBall("Am I stupid?")
// output -> Am I stupid? - answer : OMG YES!

const response = [
  "no",
  "yes",
  "maybe",
  "it's a possibility",
  "not really",
  "lol nope",
  "I think so",
  "OMG YES!",
];

const magicEightBall = () => {
  // write your code here
};

console.log(magicEightBall("Am I stupid?"))
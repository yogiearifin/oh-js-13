// this is a test about function, parameters, and string method
// make a function that accept string as the parameter and it can reverse a name of a person
// example
// input => Yogie Arifin
// output => nifirA eigoY

const reverse = () => {
  // write your code here
};

// do not edit the code below!
function Test(fun, result) {
  console.log(fun === result, `result ${fun}`);
}

Test(reverse("Kaedehara Kazuha"), "ahuzaK arahedeaK");
Test(reverse("Ren Amamiya"), "ayimamA neR");
Test(reverse("Kim Jong-un"), "nu-gnoJ miK");
Test(reverse("Jason Todd"), "ddoT nosaJ");
Test(reverse("Asep"), "pesA");

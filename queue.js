// given an array that is an order of a queue
// make a function that accepts array as a parameter and it will return the position of you
// Input => [strangers,strangers,strangers,you], [you, strangers,strangers,strangers], [strangers,strangers,strangers,you,strangers,strangers,strangers,strangers]
// Output => "you are way behind the queue", "you are in front of the queue", "you are on number 4 queue"

const strangers = "strangers";
const you = "you";

const queue = (array) => {
  //tuliskan kode disini
  //   if (array.includes(you)) {
  //     if (array[0] === you) {
  //       return "you are in front of the queue";
  //     } else if (array[array.length - 1] === you) {
  //       return "you are way behind the queue";
  //     } else {
  //       let youIndex = array.findIndex((index) => index === you);
  //       return `you are on number ${youIndex + 1} queue`;
  //     }
  //   } else {
  //     return "you are not the part of the queue";
  //   }
  let youIndex = array.findIndex((index) => index === you);
  if (youIndex >= 0) {
    if (array[0] === you) {
      return "you are in front of the queue";
    } else if (array[array.length - 1] === you) {
      return "you are way behind the queue";
    } else {
      return `you are on number ${youIndex + 1} queue`;
    }
  } else {
    return "you are not the part of the queue";
  }

  //   let youIndex = array.findIndex((index) => index === you);

  //   return array[0] === you
  //     ? "you are in front of the queue"
  //     : array[array.length - 1] === you
  //     ? "you are way behind the queue"
  //     : `you are on number ${youIndex + 1} queue`;
};

console.log(
  queue([
    strangers,
    strangers,
    strangers,
    you,
    strangers,
    strangers,
    strangers,
    strangers,
  ])
);

console.log(
  queue([
    you,
    strangers,
    strangers,
    strangers,
    strangers,
    strangers,
    strangers,
    strangers,
  ])
);

console.log(
  queue([
    strangers,
    strangers,
    strangers,
    strangers,
    strangers,
    strangers,
    strangers,
    you,
  ])
);

console.log(
  queue([
    strangers,
    strangers,
    strangers,
    strangers,
    strangers,
    strangers,
    strangers,
  ])
);

// this is a test of condition
// given two arrays of first name and last name of the eight protagonists of JoJo's Bizzare Adventure series
// make a function that accept number as the parameter and it will return the correct part of the protagonist of the series
// if the parameter is 0, it will return the name of the manga author Hirohiko Araki
// if the parameter is more than 8, it will return Yo Araki, when part NUMBER?
// if the paramter is not a number, it will return please input number!
// Input ->  JoJo(1), JoJo(0), JoJo(9), JoJo("DIO")
// Output -> Part 1 Jojo is Jonathan Joestar, Hirohiko Araki, Yo Araki, when part 9?, please input number!

const firstName = [
  "Jonathan",
  "Joseph",
  "Jotaro",
  "Part 4 Josuke",
  "Giorno",
  "Jolyne",
  "Johnny",
  "Part 8 Josuke",
];
const lastName = [
  "Joestar",
  "Joestar",
  "Kujo",
  "Higashikata",
  "Giovana",
  "Kujo",
  "Joestar",
  "Higashikata",
];

const JoJo = (num) => {
  // write code here
  if (isNaN(num)) {
    return "please input number!";
  } else {
    if (num < 9 && num > 0) {
      return `Part ${num} Jojo is ${firstName[num - 1]} ${lastName[num - 1]}`;
    } else if (num === 0) {
      return `Hirohiko Araki`;
    } else {
      return `Yo Araki, when part ${num}?`;
    }
  }
};

console.log(JoJo(9))
console.log(JoJo(8))

// do not edit the code below
function Test(fun, result) {
  console.log(fun === result, `result ${fun}`);
}

Test(JoJo(1), "Part 1 Jojo is Jonathan Joestar");
Test(JoJo(3), "Part 3 Jojo is Jotaro Kujo");
Test(JoJo(9), "Yo Araki, when part 9?");
Test(JoJo(0), "Hirohiko Araki");
Test(JoJo("DIO"), "please input number!");

// this is a test about function and parameters
// make a function to combine two different strings into one sentence
// example:
// input -> text1 = "Hello", text2 = "world"
// output -> "Hello world"

const stringJoin = (a, b) => {
    // edit your function here
};

// do not edit the codes below!
function Test(fun, result, preview) {
  console.log(fun === result, preview);
}

Test(
  stringJoin("Yogie", "Arifin"),
  "Yogie Arifin",
  `result ${stringJoin("Yogie", "Arifin")}`
);
Test(
  stringJoin("Glints", "Academy"),
  "Glints Academy",
  `result ${stringJoin("Glints", "Academy")}`
);
Test(
  stringJoin("Front", "End"),
  "Front End",
  `result ${stringJoin("Front", "End")}`
);
Test(
  stringJoin("", "Semangat!"),
  " Semangat!",
  `result ${stringJoin("", "Semangat!")}`
);

// this is a test of array method
// given the array named characters that contained the potential candidate of Suicide Squad
// as the new director, you are told to pick new squad members that can help Rick Flag and Katana
// the members must be either a villain or an anti-hero that has the power of maximum 7
// create a function that can return the name of the new squad
// input -> squad()
// output -> the new squad are Deadshot, Cheetah, Zebra Man, Rick Flag, and Katana

const characters = [
  {
    name: "Batman",
    role: "Hero",
    power: 10,
  },
  {
    name: "Robin",
    role: "Hero",
    power: 5,
  },
  {
    name: "Joker",
    role: "Villain",
    power: 8,
  },
  {
    name: "Red Hood",
    role: "Anti Hero",
    power: 9,
  },
  {
    name: "Superman",
    role: "Hero",
    power: 11,
  },
  {
    name: "Luthor",
    role: "Villain",
    power: 10,
  },
  {
    name: "Green Arrow",
    role: "Hero",
    power: 7,
  },
  {
    name: "Deadshot",
    role: "Anti Hero",
    power: 6,
  },
  {
    name: "Cheetah",
    role: "Villain",
    power: 7,
  },
  {
    name: "Deathstroke",
    role: "Anti Hero",
    power: 9,
  },
  {
    name: "Zebra Man",
    role: "Villain",
    power: 5,
  },
];

function squad() {
  // write your code here
  const newSquad = characters.filter(
    (people) =>
      people.power <= 7 &&
      (people.role === "Anti Hero" || people.role === "Villain")
  );
  console.log("newSquad", newSquad);
  const squadName = newSquad.map((item) => item.name);
  console.log("squadName", squadName);
  return `the new squad are ${squadName.join(", ")}, Rick Flag, and Katana.`;
}

// do not edit the code below
function Test(fun, result) {
  console.log(fun === result, `result ${fun}`);
}

Test(
  squad(characters),
  "the new squad are Deadshot, Cheetah, Zebra Man, Rick Flag, and Katana."
);

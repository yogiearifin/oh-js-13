// this is a test about function and parameters
// make four simple functions to calculate addition, subtraction, multiply and division of any two numbers
// example:
// input -> (2 ,2) , (4-2), (10,2), (50,10)
// output -> (4),  (2), (20), (5)

function Plus(number1, number2) {
  // edit this function
}

function Minus(n1, n2) {
  // edit this function

}

function Times(int1, int2) {
  // edit this function

}

function Divide(num, nums) {
  // edit this function

}

// do not edit the code below!
function Test(fun, result, preview) {
  console.log(fun === result, preview);
}

Test(Plus(1, 2), 3, `result ${Plus(1, 2)}`);
Test(Plus(2, -4), -2, `result ${Plus(2, -4)}`);
Test(Minus(10, 6), 4, `result ${Minus(10, 6)}`);
Test(Divide(100, 4), 25, `result ${Divide(100, 4)}`);
Test(Times(6, 7), 42, `result ${Times(6, 7)}`);

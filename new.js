// //1
// const isEven = (number) => {
//   return number % 2 === 0
//     ? `${number} is an even number`
//     : `${number} is not an even number`;
// };

// console.log(isEven(3));

// //2
// let now = new Date();

// const myDude = (day) => {
//   if (day.getDay() === 3) {
//     return "It is Wednesday, my dudes";
//   } else {
//     return "It is not Wednesday, my dudes";
//   }
// };

// console.log(myDude(now));

// //3

// const countdown = (number) => {
//   if (!isNaN(number)) {
//     for (number; number >= 0; number--) {
//       console.log(number);
//     }
//   } else {
//     console.log("not a number!");
//   }
// };

// countdown(10);

// //4
// const reversePresence = (arr) => {
//   return arr.sort();
// };

// console.log(reversePresence(["Deni", "Zulfi", "Ari", "Galih"]));

// //5
// const ingredientJoin = (array1, array2) => {
//   const result = array1.concat(array2);
//   return result;
// };

// console.log(
//   ingredientJoin(["chicken", "beef", "lamb"], ["fish", "veal", "mutton"])
// );

// //6
// const sentence = ["patuhi", "protokol", "kesehatan"];

// const sentenceFromArray = (array) => {
//   return array.join(" ");
// };

// console.log(sentenceFromArray(sentence));

// //7

// const bmiCalculator = (height, weight) => {
//   const bmi = weight / ((height / 100) * (height / 100));
//   return `your are ${
//     bmi < 18.5
//       ? "below normal"
//       : bmi >= 18.5 && bmi < 25
//       ? "normal"
//       : bmi >= 25 && bmi < 30
//       ? "overweight"
//       : bmi >= 30 && bmi < 35
//       ? "class I obesity"
//       : bmi >= 35 && bmi < 34
//       ? "class II obesity"
//       : bmi >= 40
//       ? "class III obesity"
//       : "please input the correct number"
//   }`;
// };

// console.log(bmiCalculator(180, 100));

// //8

// const amogus = (array) => {
//   let imposter = [];
//   for (let i = 0; i < array.length; i++) {
//     if (isNaN(array[i])) {
//       imposter.push(array[i]);
//     }
//   }
//   return `there are ${imposter.length} imposter among us`;
// };

// console.log(amogus([1, 2, 3, 4, "a", 5, 6, "b"]));

const arr = ["a", "b", "c", "d"];
// includes -> check if there is an item in an array -> exact checking -> ===
console.log(arr.includes("c") === "true" ? "ada c" : "tidak ada c");
console.log(arr.includes("C"));

// indexOf -> return the index of an item in an array
console.log(arr.indexOf("c"));
console.log(arr.indexOf("C"));

// toString -> return all items in an array to be a string
console.log(arr.toString());

const officeHour = [
  {
    name: "Yogie",
    age: 26,
    city: "Bandung",
  },
  {
    name: "Suto",
    age: 32,
    city: "Jakarta",
  },
  {
    name: "Balya",
    age: 24,
    city: "Medan",
  },
  {
    name: "Bima",
    age: 23,
    city: "Yogyakarta",
  },
  {
    name: "Nurdien",
    age: 28,
    city: "Bekasi",
  },
];

// higher order function -> function running with a function -> ES6

// forEach -> return all item of an array which falls into the same key

officeHour.forEach((item, index) =>
  console.log(item.name, item.age, item.city, index)
);

// map -> return all item of an array which falls into the same key -> creates new array
const officeHourCity = officeHour.map((item, index) => {
  return item.city;
});
const officeHourName = officeHour.map((item, index) => {
  return item.name;
});
const officeHourAge = officeHour.map((item, index) => {
  return item.age;
});
console.log(officeHourName, officeHourCity, officeHourAge);

const numbers = [1, 2, 3, 4, 5];
// reduce -> make a reducer , commonly used for summing array

console.log(numbers.reduce((acc, val) => acc + val)); // 1+2+3+4+5

console.log(numbers.reduce((acc, val) => acc + 5)); // 5+1+2+3+4+5

// filter -> filter all item in an array that included in the category
const ageUnderTwentyFive = officeHour.filter((item) => item.age <= 25); // [] -> if not find any item
console.log(ageUnderTwentyFive);

// find -> find the first item in an array that included in the category
const findAgeUnderTwentyFive = officeHour.find((item) => item.age <= 25); // undefined -> if not find any item
console.log(findAgeUnderTwentyFive);

// findIndex -> find the index of first item in an array that included in the category
const findIndexAgeUnderTwentyFive = officeHour.findIndex(
  (item) => item.age <= 25
); // -1 -> if not find any item
console.log(findIndexAgeUnderTwentyFive);
